from scipy.stats import norm
from csv import writer


def generate_points_hor(num_points: int = 6000):
    placement_z = norm(loc=5, scale=50)
    placement_x = norm(loc=100, scale=50)
    placement_y = norm(loc=70, scale=0.05)

    x = placement_x.rvs(size=num_points)
    y = placement_y.rvs(size=num_points)
    z = placement_z.rvs(size=num_points)

    points_hor = zip(x, y, z)
    return points_hor


def generate_points_ver(num_points: int = 6000):
    placement_z = norm(loc=0, scale=50)
    placement_x = norm(loc=100, scale=0.05)
    placement_y = norm(loc=-90, scale=50)

    x = placement_x.rvs(size=num_points)
    y = placement_y.rvs(size=num_points)
    z = placement_z.rvs(size=num_points)

    points_ver = zip(x, y, z)
    return points_ver


def generate_points_cyl(num_points: int = 15000):
    placement_z = norm(loc=50, scale=30)
    placement_x = norm(loc=-120, scale=30)
    placement_y = norm(loc=20, scale=80)

    x = placement_x.rvs(size=num_points)
    y = placement_y.rvs(size=num_points)
    z = placement_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


if __name__ == '__main__':
    chmura_punktow_ver = generate_points_ver(6000)
    with open('LidarData_pion.xyz', 'w', encoding='utf8', newline='') as csvfile:
        csvwriter = writer(csvfile)
        for p in chmura_punktow_ver:
            csvwriter.writerow(p)

    chmura_punktow_hor = generate_points_hor(6000)
    with open('LidarData_poziom.xyz', 'w', encoding='utf8', newline='') as csvfile:
        csvwriter = writer(csvfile)
        for p in chmura_punktow_hor:
            csvwriter.writerow(p)

    chmura_punktow_cyl = generate_points_cyl(15000)
    with open('LidarData_cylinder.xyz', 'w', encoding='utf8', newline='') as csvfile:
        csvwriter = writer(csvfile)
        for p in chmura_punktow_cyl:
            csvwriter.writerow(p)

    nazwy = ['LidarData_pion.xyz', 'LidarData_poziom.xyz', 'LidarData_cylinder.xyz']
    with open('LidarData_gen.xyz', 'w') as outfile:
        for fnazwy in nazwy:
            with open(fnazwy) as infile:
                for line in infile:
                    outfile.write(line)
