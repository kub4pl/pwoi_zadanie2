import csv
from matplotlib.pyplot import figure, show, title
import numpy as np
import pyransac3d as py
import random
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN


def punkty_imp():
    with open('LidarData_gen.xyz', 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for x, y, z in reader:
            yield x, y, z


point_list = []
for line in punkty_imp():
    point_list.append(line)

#RYSOWANIE 3D PUNKTÓW CHMURY - przy użyciu metody kmeans

def rysujKMeans_3d(points):
    clusterer = KMeans(n_clusters=3)

    X_clus = np.array(points, dtype=float)
    y_pred = clusterer.fit_predict(X_clus)

    red = y_pred == 0
    blue = y_pred == 1
    yellow = y_pred == 2

    fig = figure(figsize=(5, 5))
    ax = fig.add_subplot(projection='3d')

    ax.scatter(X_clus[red, 0], X_clus[red, 1], X_clus[red, 2], c="r")
    ax.scatter(X_clus[blue, 0], X_clus[blue, 1], X_clus[blue, 2], c="b")
    ax.scatter(X_clus[yellow, 0], X_clus[yellow, 1], X_clus[yellow, 2], c="y")
    title('KMeans algorytm')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    show()


rysujKMeans_3d(point_list)


# Algorytm ransac przy uzyciu chmury punktow

def alg_ransac(points, threshold):
    X = np.array(points, dtype=float)
    A = random.choice(X)
    B = random.choice(X)
    C = random.choice(X)

    vector_A = np.subtract(A, C)
    vector_B = np.subtract(B, C)

    vector_Ua = vector_A / np.linalg.norm(vector_A)
    vector_Ub = vector_B / np.linalg.norm(vector_B)

    n = np.cross(vector_Ua, vector_Ub)

    d = -np.sum(np.multiply(C, n))

    dystans_ob = (n[0] * X[:, 0] + n[1] * X[:, 1] + n[2] * X[:, 2]) + d

    inliers = np.where(np.abs(dystans_ob) <= threshold)[0]

    print('Wspoczynniki dla plaszczyzny wynosza: ', '\na: ', n[0], '\nb: ', n[1], '\nc: ', n[2], '\nd: ', d,
          '\n\nLiczba inlinerow wynosi: ', inliers)

    if n[1] == 0 and n[0] == 0 and n[2] != 0:
        print('Plaszczyzna jest pinowa.', end='')
    elif (n[0] != 0 or n[1] != 0) and n[2] == 0:
        print('Plaszczyzna jest pozioma.', end='')
    else:
        print('Ani pinowa ani pozioma.')


alg_ransac(point_list, 0.01)


# Rysowanie 3d chmury punktów - metoda dbscan

def drawDBSCAN_3d(points, eps, minimum_samples):
    X_clus = np.array(points, dtype=float)
    clusterer = DBSCAN(eps, min_samples=minimum_samples)
    y_pred = clusterer.fit_predict(X_clus)

    red = y_pred == 0
    blue = y_pred == 1
    yellow = y_pred == 2

    fig = figure(figsize=(5, 5))
    ax = fig.add_subplot(projection='3d')

    ax.scatter(X_clus[red, 0], X_clus[red, 1], X_clus[red, 2], c="r")
    ax.scatter(X_clus[blue, 0], X_clus[blue, 1], X_clus[blue, 2], c="b")
    ax.scatter(X_clus[yellow, 0], X_clus[yellow, 1], X_clus[yellow, 2], c="y")
    title('DBSCAN algorytm')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    show()


drawDBSCAN_3d(point_list, 40, 500)


# Algorytm raansac do chmury punktow

def ransac3d_plaszczyzna(points):
    X = np.array(points, dtype=float)
    plaszczyzna = py.Plane()
    best_eq, best_inliers = plaszczyzna.fit(X, 0.01)
    print('\n\nWyswietl wspolczynniki plaszczyzny:', '\na: ', best_eq[0], '\nb: ', best_eq[1], '\nc: ', best_eq[2],
          '\nd: ', best_eq[3], '\n')


ransac3d_plaszczyzna(point_list)
